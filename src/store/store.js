import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    showModal: {
      add: false,
      edit: false,
      delete: false
    },
    message: {
      success: false,
      failed: false
    },
    customers: null,
    editActiveCustomer: {},
    delActiveCustomer: {},
    beforeEditingCache: {}
  },
  mutations: {
    TOGGLE_MODAL (state, action) {
      state.showModal[action] = !state.showModal[action]
    },
    TOGGLE_MESSAGE (state, action) {
      state.message[action] = !state.message[action]
    },
    SHOW_CUSTOMERS (state, data) {
      state.customers = data
    },
    UPDATE_CUSTOMER (state, customerId) {
      state.editActiveCustomer = state.customers.find(customer => customer.id === customerId)
      // Copia de objeto para posteriormente cancelar edición de formulario de Edición
      state.beforeEditingCache = Object.assign({}, state.editActiveCustomer)
    },
    DEL_CUSTOMER (state, customerId) {
      state.delActiveCustomer = state.customers.find(customer => customer.id === customerId)
    }
  }
})
