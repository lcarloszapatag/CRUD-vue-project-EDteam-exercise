import Vue from 'vue'
import Router from 'vue-router'

import Customers from '../components/Customers.vue'
import About from '../components/About.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'customers', component: Customers },
    { path: '/about', name: 'about', component: About }
  ]
})
